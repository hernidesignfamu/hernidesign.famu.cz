const fs = require('fs')
const path = require('path')

const getAllRoutesInCollection = (collectionName) => {
    return fs.readdirSync(`./assets/content/${collectionName}`).map(file => {
        return {
            slug: `${path.parse(file).name}`,
            payload: require(`../assets/content/${collectionName}/${file}`),
        };
    });
}

const generateRoutes = () => {
    const ret = []

    // Slugs
    const staticPages = getAllRoutesInCollection('static_page')
    const articles = getAllRoutesInCollection('articles')
    const games = getAllRoutesInCollection('games')
    const people = getAllRoutesInCollection('people')

    // CS
    staticPages.map(r => {
        ret.push({
            route: `/${r.slug}`,
            payload: r.payload
        })
    })
    articles.map(r => {
        ret.push({
            route: `/clanek/${r.slug}`,
            payload: r.payload
        })
    })
    games.map(r => {
        ret.push({
            route: `/hra/${r.slug}`,
            payload: r.payload
        })
    })
    people.map(r => {
        ret.push({
            route: `/profil/${r.slug}`,
            payload: r.payload
        })
    })

    return ret
}

module.exports = generateRoutes
