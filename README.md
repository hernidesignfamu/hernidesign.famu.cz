# hernidesign.famu.cz

> Game design @ FAMU web pages source

## Concept

hernidesign.famu.cz ("the site" hereafter) is a statically generated Nuxt.js site, using Netlify CMS for content management.

## Principles

1. The purpose of the site is to:
   1. Provide information about the study programme to public, prospective students and current students and faculty
   2. Provide a space to present student games and initiatives

## Design

1. The site follows the established visual language of [FAMU](http://www.famu.cz)
2. The site deviates from the layout used on FAMU homepage and implements custom UI
3. The site is accessible on any device with screen width larger than 320px
4. The site is accessible to visually impaired visitors

## Languages

The site curently supports Czech (cs) and English (en) language mutations using nuxt-i18n and language namespaced Netlify CMS collections.

## Development

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev
```

## Deployment

```bash
yarn generate
```

The generated /dist directory is a static snapshot that can be uploaded to a static distribution (S3, Azure Files), CDN (CloudFront) or a standard web server (nginx, Apache).

## External docs

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
