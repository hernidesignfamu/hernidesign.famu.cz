export default {
  target: 'static',
  server: {
    port: process.env.PORT || 3000
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Herní design FAMU',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Oficiální stránky oboru Herní design na pražské FAMU.' },

      // OpenGraph
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:image', property: 'og:image', content: 'https://hernidesign.famu.cz/images/og/default.jpg' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Archivo:wght@400;500;600;700&family=DM+Sans:wght@400;500&display=swap'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;700&display=swap' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/global'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/moment',
    '@nuxt/typescript-build',
    '@nuxt/image'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // I18n
    [
      'nuxt-i18n',
      {
        locales: ['cs', 'en'],
        defaultLocale: 'cs',
        vueI18n: {
          fallbackLocale: 'cs',
          messages: {
            cs: {},
            en: {}
          }
        }
      }
    ],
    // Nuxt Content
    '@nuxt/content',
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    //
    '@nuxtjs/markdownit'
  ],
  content: {
    dir: 'assets/content'
  },
  markdownit: {
    injected: true,
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
