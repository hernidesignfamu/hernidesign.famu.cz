declare namespace FhdNetlifyCollections {
    interface StaticPage {
        language: string
        title: string
        sub_title: string
        title_image: string
        updated_at: string
        body: string
        tags: string[]
    }

    interface Menu {
        language: string
        id: string
        sections: any[]
    }

    interface Game {
        slug: string
        language: string
        about: string
        main_image: string
        name: string
        release_date: string
        screenshots: string[]        
    }

    interface Person {
        language: string
        type: 'faculty' | 'student' | 'alumni'
        name: string
        titles_before_name: string[]
        titles_after_name: string[]
        position: string
        short_bio: string
        bio: string
        photo: string
        list_priority: number
    }

    interface Event {
        language: 'cs' | 'en'
        title: string
        description: string
        event_date: string
    }
}